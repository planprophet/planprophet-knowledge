({ 
  loadKnowledgeData: function (component, helper) {
    var categGroup = component.get("v.categoryGroup");
    var params = {
      categoryGroup: categGroup
    };

    this._invoke(
      component,
      "c.getCategoryGroupTree",
      params,
      function (result) {
        if (result.length > 0) {         
          var resultCopy = JSON.parse(JSON.stringify(result));
          console.log('Article Id ' + component.get("v.articleId"));
          var articleId = component.get("v.articleId");
          //make a deep copy for new reference
          var itemSelected = JSON.parse(JSON.stringify(result[0]));
          itemSelected.found = false;
        
          if(articleId != null && articleId != ''){           
            try{
              var valid = helper.validateArticleId(articleId)
              if(valid)
                helper.findArticleInItems(result, articleId, itemSelected);            
            }catch(e){
              if (e.message == "I found the element"){
                  console.log("I Found the element" + itemSelected.name);    
              }
              else console.log(e);
            }                    
          }
          else{
            result[0].expanded = true;
          }        
                          
          var breadcrumbsList = [];
          try {          
            helper.fillBreadCrumbList(breadcrumbsList, result, itemSelected.name);
          } catch (err) {
              if (err.message == "I found the element"){
                  component.set("v.breadcrumbCollection", breadcrumbsList);      
              }
              else console.log(err);
          }
         component.set("v.dataCategoryStructure", result);     
         component.set("v.resultServer", resultCopy);     
        // console.log("Article URL " + itemSelected.articleUrlName);
         component.set("v.urlName", itemSelected.articleUrlName);
         component.set("v.articleLabel", itemSelected.label);
         component.set("v.showWarning",  (articleId != null && articleId != '')? !itemSelected.found : false );  
         component.set("v.loading", false);
         component.set("v.selectedItem", itemSelected.name); 
        }
       
      },
      function (error) {
        console.log("Error " + error);
      }
    );
  },

  fillBreadCrumbList: function (breadCrumbList, itemList, selectedItemName) {
     itemList.forEach((element) => {
      if (element.name == selectedItemName) {     
        breadCrumbList.push({ label: element.label, name: element.name });
        throw new Error("I found the element");
      } else {
        var selectedItemSplit = selectedItemName.split("-");
        var selectedItemLength = selectedItemSplit.length;
        var elementNameSplit = element.name.split("-");
        var elementNameLength = elementNameSplit.length;

        if (selectedItemLength > elementNameLength) {
          if (selectedItemName.includes(element.name)) {
            breadCrumbList.push({ label: element.label, name: element.name });
            this.fillBreadCrumbList(
              breadCrumbList,
              element.items,
              selectedItemName
            );
          } else {
            console.log("Is not my branch");
          }
        } else {
          console.log("We are brothers");
        }
      }
    });  
  },

  findArticleInItems: function (itemList, articleId, itemSelected) {  
    itemList.forEach((element) => {  
      if (element.name.includes(articleId)) {    
        element.expanded = true;  
        itemSelected.name = element.name;   
        itemSelected.found = true;      
        itemSelected.articleUrlName = element.articleUrlName;    
        itemSelected.label = element.label;    
         throw new Error("I found the element");      
      }       
      this.findArticleInItems(element.items,articleId, itemSelected);
    });  
  },

  /*setItemExpanded: function (itemList, selectedItemName) {
    itemList.forEach((element) => {
      if (element.name == selectedItemName) {           
         element.expanded = true;
         throw new Error("I found the element");
      }     
      this.setItemExpanded(element.items,selectedItemName);
    });  
  },

  resetItemExpanded: function (itemList) {
    itemList.forEach((element) => {
      element.expanded = false; 
      this.resetItemExpanded(element.items);
    });  
  },
  
  handleExpandedItem: function (component, selectedItemName){   
    var itemsList = component.get("v.dataCategoryStructure");
    this.resetItemExpanded(itemsList);
    component.set("v.dataCategoryStructure", itemsList);

    try {    
      this.setItemExpanded( itemsList, selectedItemName);
    } catch (e) {
      if (e.message == "I found the element"){     
         component.set("v.dataCategoryStructure", itemsList);
      }       
       else console.log(e);
    }
  },*/

  reloadBreadCrumbs: function (component, selectedItemName) {
    var breadcrumbsList = [];
    var itemsList = component.get("v.dataCategoryStructure");
    //  var found = false
    try {
      this.fillBreadCrumbList(breadcrumbsList, itemsList, selectedItemName);
    } catch (e) {
      if (e.message == "I found the element"){
         component.set("v.breadcrumbCollection", breadcrumbsList);      
      }
       
      else console.log(e);
    }
  },
  updateBreadCrumbsList: function (cmp, selectedItemName) {
 
    var breadcrumbsList = cmp.get("v.breadcrumbCollection");
    var breadCrumbNewList = [];
    try {
      breadcrumbsList.forEach((element) => {
        breadCrumbNewList.push(element);
        if (element.name == selectedItemName) {
          throw new Error("I found the element");
        }
      });
    } catch(e) {
      console.log(e.message)
      if (e.message == "I found the element"){
         cmp.set("v.breadcrumbCollection", breadCrumbNewList);
         cmp.set("v.selectedItem", selectedItemName);
      }
       
      else console.log(e);
    }
    console.log("List Breadcrumb NEW " + JSON.stringify(breadCrumbNewList));
  },

  hideWarning : function(component){
    component.set("v.showWarning", false); 
  },

  reloadArticlePage: function(component){
    console.log("4. Reload Page Actual URL " +  window.location.pathname);
    var urlCurrent =  window.location.pathname;
    var urlArray = urlCurrent.split('/');   

    var articleId =  component.get("v.articleId");    
    var articleUrl =  component.get("v.urlName");    
    var articleLabel =  component.get("v.articleLabel");    

   // urlArray[urlArray.length - 1] = articleId;
    urlArray[urlArray.length - 1] = articleUrl;

    var urlRedirect = urlArray.join('/');
    console.log("5. Reload to New URL " + urlRedirect)
   // component.set("v.urlName", 'Creating-a-New-Prospect');
   /*RELOAD*/
   // window.location.pathname = urlRedirect;

   /** UPDATE URL without reload */
    window.history.pushState({ urlName: articleUrl , recordId : articleId}, articleLabel , urlRedirect);
    document.title = articleLabel;
    
    /* Components refresh View */
   /* var urlEvent = $A.get("e.force:refreshView");  
    urlEvent.fire(); */
  },

  changeArticleId: function(component, itemName){
    
    var itemNameSplit = itemName.split('-');
    var articleId = itemNameSplit.length > 0 ? itemNameSplit[0] : '';
    console.log("2. Change Article ID-----------" + ' Before ' + component.get("v.articleId") + ' AFTER ' + articleId);
    
   
    var itemList =  component.get("v.resultServer");
    var itemSelected = JSON.parse(JSON.stringify(itemList[0]));
    itemSelected.found = false;
  
    if(articleId != null && articleId != ''){           
      try{
        var valid = this.validateArticleId(articleId)
        if(valid)
          this.findArticleInItems(itemList, articleId, itemSelected); 
      }catch(e){
        if (e.message == "I found the element"){
           console.log("Update URL NAME ----" + itemSelected.articleUrlName);
           console.log("3. Change URL NAME -----------" + ' Before ' + component.get("v.urlName") + ' AFTER ' + itemSelected.articleUrlName);
           component.set("v.urlName", itemSelected.articleUrlName);
           component.set("v.articleLabel", itemSelected.label);
           component.set("v.articleId", articleId);  
        }
         else console.log(e);
      }
    }
  },

  requireReload: function(component, itemName){      
      var itemNameSplit = itemName.split('-');
      var articleId = itemNameSplit.length > 0 ? itemNameSplit[0] : '';
     
      var articleParameter = component.get("v.articleId");
      //if we are in Article Detail page so the component has specified the article Id
      if(articleParameter != null && articleParameter != ''){ 
        var valid = this.validateArticleId(articleId);
        if(valid && itemNameSplit[itemNameSplit.length - 1] == 'ImAnArticle') {
               return true;          
        }   
      }
      return false;
   },

   validateArticleId : function(articleId){
    if(articleId != '' ){
       var regex;
       regex = new RegExp('[k]{1}[a-z]{1}[0-9]{1}([a-zA-Z0-9]{15}|[a-zA-Z0-9]{12})');
       if(regex.test(articleId)) {
         return true;        
       }      
     }   
     return false;
   },

   _invoke: function(component, methodName, parameters, onSuccess, onError) {
   var action, errors, e;
   action = component.get(methodName);

   if (parameters)
     action.setParams(parameters);

   action.setCallback(this, function(response) {
     switch (response.getState()) {
       case "SUCCESS":
         onSuccess(response.getReturnValue());
         break;
       case "ERROR":				
         errors = response.getError();
         e = (errors && errors[0] && errors[0].message) || "Unknow Error";
         (onError ? onError : console.error)(e);
     }
   });
   $A.enqueueAction(action);
 },
 	
})