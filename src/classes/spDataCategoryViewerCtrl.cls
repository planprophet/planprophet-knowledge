/**
 * @description       : 
 * @author            : dmorales
 * @group             : 
 * @last modified on  : 10-23-2020
 * @last modified by  : dmorales
 * Modifications Log 
 * Ver   Date         Author     Modification
 * 1.0   10-06-2020   dmorales   Initial Version
**/
public without sharing class spDataCategoryViewerCtrl {

    @AuraEnabled
    public static List<spDataCategoriesHelper.ItemWrapper> getCategoryGroupTree(String categoryGroup) {
       
     //  List<spDataCategoriesHelper.ItemWrapper> treeStructure = spDataCategoriesHelper.getCategoryGroupTree(categoryGroup);
 
       List<spDataCategoriesHelper.ItemWrapper> treeStructure = spDataCategoriesHelper.getCategoryTreeWithArticles(categoryGroup);
       return treeStructure;
    }

    @AuraEnabled
    public static List<KnowledgeArticleVersion> getArticlesUrlByCateg(String categoryGroup, String dataCategory) {
       
        List<KnowledgeArticleVersion> articleList = spDataCategoriesHelper.getArticlesUrlByCateg(categoryGroup,dataCategory);          
        return articleList;
    }
     
}