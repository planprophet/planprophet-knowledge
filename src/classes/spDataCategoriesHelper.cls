/**
 * @description       :
 * @author            : dmorales
 * @group             :
 * @last modified on  : 10-22-2020
 * @last modified by  : dmorales
 * Modifications Log
 * Ver   Date         Author     Modification
 * 1.0   10-06-2020   dmorales   Initial Version
 **/
public without sharing class spDataCategoriesHelper {

    public static ItemWrapper getDataCategoryGroupItem(
        String sObjectName,
        String groupName,
        String groupLabel
        ) {

        if (String.isBlank(sObjectName) || String.isBlank(groupName)) {
            return null;
        }
        //else...
        List<DataCategory> topLevelCategories = getTopCategories(sObjectName, groupName);
        if (topLevelCategories == null)
            return null;
        //else
        DataCategory topCat = topLevelCategories[0];
        ItemWrapper result = new ItemWrapper(groupLabel,topCat.getName(),groupName);
        addCategoryItems(result, topCat.getChildCategories());

        return result;
    }

    private static void addCategoryItems(
        ItemWrapper itemMaster,
        List<DataCategory> categories
        ) {
        for(DataCategory cat : categories) {
            ItemWrapper child = new ItemWrapper(
                cat.getLabel(),
                cat.getName(),
                itemMaster.name);
            itemMaster.items.add(child);

            List<DataCategory> childCategories = cat.getChildCategories();
            addCategoryItems(child, childCategories);
        }
    }

    public static List<DataCategory> getTopCategories(String sObjectName, String groupName) {
        List<DescribeDataCategoryGroupStructureResult> describeCategoryStructureResult =
            getDescribeCategoryStructureResult(sObjectName, groupName);
        System.debug('describeCategoryStructureResult.size: ' + describeCategoryStructureResult.size());
        if (describeCategoryStructureResult.isEmpty()) {
            return null;
        }
        //else...
        DescribeDataCategoryGroupStructureResult singleResult = describeCategoryStructureResult[0];
        //Get the top level categories
        List<DataCategory> topLevelCategories = singleResult.getTopCategories();
        System.debug('topLevelCategories.size: ' + topLevelCategories.size());
        return topLevelCategories;
    }   
    
    public static DataCategory getTopDataCategory(String sObjectName, String groupName) {
       
        List<DescribeDataCategoryGroupStructureResult> describeCategoryStructureResult = spDataCategoriesHelper.getDescribeCategoryStructureResult(sObjectName, groupName);
        if (describeCategoryStructureResult.isEmpty()) {
            return null;
        }
        //else
        DescribeDataCategoryGroupStructureResult singleResult = describeCategoryStructureResult[0]; //Category Group element
        //Get the top level categories
        List<DataCategory> topLevelCategories = singleResult.getTopCategories();        
        DataCategory topCat = topLevelCategories[0]; //Ex. All - default top level Data Category
        return topCat;
    }

    public static List<DescribeDataCategoryGroupStructureResult> getDescribeCategoryStructureResult(String sObjectName, String groupName) {
        List<DataCategoryGroupSobjectTypePair> pairs = new List<DataCategoryGroupSobjectTypePair>();
        DataCategoryGroupSobjectTypePair p = new DataCategoryGroupSobjectTypePair();
        p.setSobject(sObjectName);
        p.setDataCategoryGroupName(groupName);
        pairs.add(p);
        List<DescribeDataCategoryGroupStructureResult> describeCategoryStructureResult =
            Schema.describeDataCategoryGroupStructures(pairs, false);
        return describeCategoryStructureResult;
    }

    public static List<KnowledgeArticleVersion> getArticlesUrlByCateg(String categoryGroup, String dataCategory) {

        String strCategoryGroup = String.escapeSingleQuotes(categoryGroup);
        String strDataCategory = String.escapeSingleQuotes(dataCategory);
        String query = ' SELECT Id, KnowledgeArticleId, Title, UrlName, Summary, PublishStatus'+
                       ' FROM KnowledgeArticleVersion ' +
                       ' WHERE PublishStatus = \'Online\''+
                       ' WITH Data Category '+ strCategoryGroup +'__c'  +
                       ' AT '+strDataCategory +'__c ' +
                       ' ORDER BY Title  ';
        List<KnowledgeArticleVersion> articleList =  Database.query(query);
        return articleList;
    }

    private static void buildArticleMap(List<ItemWrapper> items, Map<String, List<ItemWrapper> > mapArticles){

        for(ItemWrapper item: items) {
            mapArticles.put(item.name, new List<ItemWrapper>());
            buildArticleMap(item.items,mapArticles);
        }
    }

    public static Map<String, List<ItemWrapper> > getArticlesCategoryMap(List<ItemWrapper> items){
        Map<String, List<ItemWrapper> > mapArticles = new Map<String, List<ItemWrapper> > ();
        buildArticleMap(items, mapArticles);

        return mapArticles;
    }

    public static List<ItemWrapper> getCategoryTree() {

        List<String> objName = new List<String> {spDataCategoriesConstants.ARTICLE_SOBJECT_NAME};
        List<Schema.DescribeDataCategoryGroupResult> results = Schema.describeDataCategoryGroups(objName);

        List<ItemWrapper> treeStructure = new List<ItemWrapper>();
        for(Schema.DescribeDataCategoryGroupResult r: results) {
            ItemWrapper newObj = getDataCategoryGroupItem(spDataCategoriesConstants.ARTICLE_SOBJECT_NAME, r.name, r.label);
            treeStructure.add(newObj);
        }

        return treeStructure;
    }

    public static List<ItemWrapper> getCategoryGroupTree(String categoryGroup) {

        List<String> objName = new List<String> {spDataCategoriesConstants.ARTICLE_SOBJECT_NAME};
        List<Schema.DescribeDataCategoryGroupResult> results = Schema.describeDataCategoryGroups(objName);

        List<ItemWrapper> treeStructure = new List<ItemWrapper>();
        Boolean found = false;
        Integer i = 0;
        while(!found && i < results.size()) {
            Schema.DescribeDataCategoryGroupResult r = results[i];
            if(r.name == categoryGroup) {
                found = true;
                ItemWrapper newObj = getDataCategoryGroupItem(spDataCategoriesConstants.ARTICLE_SOBJECT_NAME, r.name, r.label);
                treeStructure.add(newObj);
            }
            i++;
        }
        return treeStructure;
    }

    public static List<ItemWrapper> getCategoryTreeWithArticles(String categoryGroup) {
        List<ItemWrapper> categoryTree = new  List<ItemWrapper> ();
        if(!String.isEmpty(categoryGroup)){
            categoryTree = getCategoryGroupTree(categoryGroup);
        }
        else{
            categoryTree = getCategoryTree();
        }
      
        Map<String, List<ItemWrapper> > mapTree = getArticlesCategoryMap(categoryTree);
        Integer mapElements = mapTree.size();
        System.debug('Size of map  '  + mapElements);
        //To Do...obtain SOQL left
        if(mapElements < 150) {
            Set <String> mapKeys = new Set<String>();
            mapKeys = mapTree.keySet();
            for(String key : mapKeys) {
                String[] categNames  = key.split('-');              
                String categName = categNames[0];
                String categGroup = categNames[categNames.size() - 1];
                List<KnowledgeArticleVersion> articles = getArticlesUrlByCateg(categGroup, categName);
                System.debug('Size of articles of ' + categName + '-' + categGroup);
                List<ItemWrapper> articlesItems = convertArticleToItem(articles, key + '-ImAnArticle');
                System.debug('Size after convert ' + articlesItems.size());
                mapTree.put(key, articlesItems);
            }
        }

        addArticlesToItems(categoryTree, mapTree);

        return categoryTree;
    }

    public static List<ItemWrapper> convertArticleToItem(List<KnowledgeArticleVersion> articles, String parent){
        List<ItemWrapper> itemList = new List<ItemWrapper>();
        for(KnowledgeArticleVersion article : articles) {
            ItemWrapper newItem =  new ItemWrapper(article.Title, article.Id, parent);
            newItem.articleUrlName = article.UrlName;
            itemList.add(newItem);
        }
        return itemList;
    }

    public static void addArticlesToItems(List<ItemWrapper> categoryTree, Map<String, List<ItemWrapper> > mapTree){
        for(ItemWrapper item : categoryTree) {
            List<ItemWrapper> itemsFromMap = mapTree.get(item.name);
            if(itemsFromMap == null){
                continue;
            }              
            System.debug('Inside  ' + itemsFromMap.size());
            System.debug('Inside   ' + itemsFromMap);
            item.items.addAll(itemsFromMap);
            addArticlesToItems(item.items,mapTree);
        }
    }


    public class ItemWrapper {
        @AuraEnabled
        public String label {get; set;}
        @AuraEnabled
        public String name {get; set;}
        @AuraEnabled
        public String categoryParent {get; set;}
        @AuraEnabled
        public Boolean disable {get; set;}
        @AuraEnabled
        public Boolean expanded {get; set;}
        @AuraEnabled
        public String articleUrlName {get; set;}
        @AuraEnabled
        public List<ItemWrapper> items {get; set;}

        public ItemWrapper(String label, String name, String categoryParent) {
            this.name = name+'-'+categoryParent;
            this.label = label;
            this.categoryParent = categoryParent;
            this.disable = false;
            this.expanded = false;
            this.articleUrlName = '';
            this.items = new List<ItemWrapper>();
        }
    }
}