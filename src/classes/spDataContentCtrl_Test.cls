/**
 * @description       :
 * @author            : dmorales
 * @group             :
 * @last modified on  : 10-23-2020
 * @last modified by  : dmorales
 * Modifications Log
 * Ver   Date         Author     Modification
 * 1.0   10-23-2020   dmorales   Initial Version
 **/
@isTest
private class spDataContentCtrl_Test {

    static String createArticle(){
        Knowledge__kav articleObj = new Knowledge__kav(
            Title = 'Test Article pp 3',
            UrlName = 'Test-article-pp-3',
            Body__c = 'Hello World'
            );
        insert articleObj;
        return articleObj.Id;
    }
   
    @isTest
    static void test_getContentForDataCategory() {
   
        List<DescribeDataCategoryGroupStructureResult> describeCategoryStructureResult =
        spDataCategoriesHelper.getDescribeCategoryStructureResult(spDataCategoriesConstants.ARTICLE_SOBJECT_NAME, 
                                                                  spDataCategoriesConstants.DATA_CATEGORY_NAME);
        if (!describeCategoryStructureResult.isEmpty()) {
            DescribeDataCategoryGroupStructureResult singleResult = describeCategoryStructureResult[0]; //Category Group element
            //Get the top level categories
            List<DataCategory> topLevelCategories = singleResult.getTopCategories();
            DataCategory topCat = topLevelCategories[0];
            List<DataCategory> firstLevelList = topCat.getChildCategories();
            if(firstLevelList.size()> 0){
                topCat = firstLevelList[0];
            }            
           
            spContentViewerCtrl.ContentGroupWrapper contentW = 
               spContentViewerCtrl.getContentForDataCategory(spDataCategoriesConstants.DATA_CATEGORY_NAME, topCat.getName());
            System.assert (contentW != null, 'No Content Created');
        }
    } 
   
    @isTest
    static void test_getContentForCategoryGroup() {
        List<String> objName = new List<String> {spDataCategoriesConstants.ARTICLE_SOBJECT_NAME};
        List<Schema.DescribeDataCategoryGroupResult> results = Schema.describeDataCategoryGroups(objName);
        if(results != null && results.size() > 0) {
            Schema.DescribeDataCategoryGroupResult firstGroup = results[0];
            spContentViewerCtrl.ContentGroupWrapper contentW = spContentViewerCtrl.getContentForCategoryGroup(firstGroup.getName());
            System.assert (contentW != null, 'No Content created');
        }

    } 
    
    @isTest
    static void test_getContentForArticle() {
          String articleId = createArticle();
          spContentViewerCtrl.ContentGroupWrapper contentW = 
             spContentViewerCtrl.getContentForArticle(articleId, 
                                                      spDataCategoriesConstants.KNOWLEDGE_NAME, 
                                                      spDataCategoriesConstants.ARTICLE_BODY_NAME);
          System.assert (contentW != null, 'No Content Created');                                           
    }
}